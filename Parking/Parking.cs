﻿using System.Linq;
using Parking.Vehicles;
using System.Collections.Generic;
using System.Threading;
using System;

namespace Parking
{
    public class Parking
    {
        #region Static Properties

        public static int MaximumParkingCapacity { get; set; }
        public static double InitialParkingBalance { get; set; }
        public static int PaymentPeriodicity { get; set; }
        public static double PaymentFineCoefficient { get; private set; } = 1.5;

        public static double BusTariff { get; set; } = 3.5;
        public static double CarTariff { get; set; } = 2.0;
        public static double MotorcycleTariff { get; set; } = 1.0;
        public static double TruckTariff { get; set; } = 5.0;

        #endregion

        #region Public Properties

        public int MaximumCapacity { get; private set; } = MaximumParkingCapacity;
        public double Balance { get; private set; } = InitialParkingBalance;
        public double FineCoefficient { get; private set; } = PaymentFineCoefficient;
        public int Capacity => vehicles.Count();

        public IEnumerable<IVehicle> Vehicles =>
            Vehicles;
        private volatile List<IVehicle> vehicles = new List<IVehicle>();

        public IEnumerable<Transaction> Transactions =>
            transactions;
        private volatile List<Transaction> transactions = new List<Transaction>();

        #endregion

        #region Singleton

        private Parking(ParkingSettings settings)
        {
            MaximumParkingCapacity = settings.MaximumCapacity;
            InitialParkingBalance = settings.InitialBalance;
            PaymentPeriodicity = settings.PaymentPeriodicity;
            PaymentFineCoefficient = settings.FineCoefficient;

            BusTariff = settings.BusTariff;
            CarTariff = settings.CarTariff;
            MotorcycleTariff = settings.MotorcycleTariff;
            TruckTariff = settings.TruckTariff;

            new Thread(_collectTaxes).Start();
            new Thread(_removeTaxes).Start();
        }
        private static Parking _parking = null;
        public static Parking GetParking() =>
            _parking ?? (_parking = new Parking(SettingsManager.Read()));

        public static void ClearSingleton() =>
            _parking = null;

        public static bool ChangeSettings(ParkingSettings settings)
        {
            SettingsManager.Write(settings);

            MaximumParkingCapacity = settings.MaximumCapacity;
            InitialParkingBalance = settings.InitialBalance;
            PaymentPeriodicity = settings.PaymentPeriodicity;
            PaymentFineCoefficient = settings.FineCoefficient;

            BusTariff = settings.BusTariff;
            CarTariff = settings.CarTariff;
            MotorcycleTariff = settings.MotorcycleTariff;
            TruckTariff = settings.TruckTariff;

            return true;
        }

        #endregion

        #region Public Methods

        public bool Add(IVehicle vehicle)
        {
            if (vehicles.Count == MaximumCapacity) return false;
            vehicles.Add(vehicle);
            return true;
        }

        public bool Remove(IVehicle vehicle) =>
            vehicles.Remove(vehicle);

        #endregion

        #region Private Methods

        private void CollectTaxes()
        {
            if (vehicles.Count == 0) return;
            lock (transactions)
            {
                foreach (var vehicle in Vehicles)
                    transactions.Add(CollectTax(vehicle));
            }
        }
            
        private Transaction CollectTax(IVehicle vehicle)
        {
            var price = 0.0;
            switch (vehicle.VehicleType)
            {
                case VehicleTypes.Bus:
                    price = BusTariff;
                    break;
                case VehicleTypes.Car:
                    price = CarTariff;
                    break;
                case VehicleTypes.Motorcycle:
                    price = MotorcycleTariff;
                    break;
                case VehicleTypes.Truck:
                    price = TruckTariff;
                    break;
            }
            var paid = vehicle.Pay(price, FineCoefficient);
            Balance += paid;
            return new Transaction(vehicle.Id, paid);
        }

        private void _collectTaxes()
        {
            var time = PaymentPeriodicity * 1000;
            while (true)
            {
                Thread.Sleep(time);
                CollectTaxes();
            }
        }
        private void _removeTaxes()
        {
            while (true)
            {
                Thread.Sleep(1000);
                foreach (var tra in Transactions)
                    if (tra.TransactionDateTime.AddMinutes(5) < DateTime.Now)
                        transactions.Remove(tra);
            }
        }

        #endregion
    }
}
