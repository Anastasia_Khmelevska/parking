﻿using Parking.Vehicles;
using System;
using System.Collections.Generic;

namespace Parking
{
    class Program
    {
        static void Main(string[] args)
        {
            //var veh = new Vehicle(10.0, VehicleTypes.Bus);
            //var vehicles = new List<IVehicle>
            //{
            //    new Vehicle(10.0, VehicleTypes.Bus),
            //    new Vehicle(12.0, VehicleTypes.Car),
            //    new Vehicle(50.0, VehicleTypes.Motorcycle),
            //    new Vehicle(212.0, VehicleTypes.Truck)
            //};
            //VehicleManager.Write(vehicles);

            new UserInterface().Run();
        }
    }
}
