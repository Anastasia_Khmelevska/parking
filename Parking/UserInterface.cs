﻿using Parking.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Parking
{
    public class UserInterface
    {
        #region Entry Point

        public void Run()
        {
            while (true)
            {
                var point = NavigationList("MENU", new string[]
                {
                    "Parking Settings",
                    "Run Parking",
                    "Reload Parking",
                    "EXIT"
                }, 3);
                switch (point)
                {
                    case 0:
                        ParkingSettings();
                        continue;
                    case 1:
                        ParkingMenu();
                        continue;
                    case 2:
                        ReloadParking();
                        continue;
                    default:
                        break;
                }
                break;
            }

            Console.Write("\nPress to out..."); Console.ReadKey();
        }
        private void ReloadParking()
        {
            Console.Clear();
            Console.Write("Are you shure? (y/n) ");
            var key = Console.ReadKey();
            if (key.Key == ConsoleKey.Y)
            {
                Parking.ClearSingleton();
                Console.WriteLine("\nCleared!");
                Console.ReadKey();
                return;
            }
            Console.WriteLine("\nNot cleared!");
            Console.ReadKey();
        }
        #endregion
        #region Parking
        public void ParkingMenu()
        {
            while (true)
            {
                var point = NavigationList("PARKING (Capacity: " + Parking.GetParking().Capacity + " | Balance: " + Parking.GetParking().Balance + ")", new string[]
                {
                    "Add Vehicle",
                    "Parking storage",
                    "Show Transactions",
                    "BACK"
                }, 3);
                switch (point)
                {
                    case 0:
                        AddVehicle();
                        continue;
                    case 1:
                        continue;
                    case 2:
                        ShowTransactions();
                        continue;
                    default:
                        break;
                }
                break;
            }
        }
        private void AddVehicle()
        {
            var vehicles = VehicleManager.Read().ToList();
            while (true)
            {
                var lines = new List<string>();
                lines.AddRange(vehicles.Select(vehicle => vehicle.ToString()));
                lines.Add("BACK");

                var point = NavigationList("ADD VEHICLE", lines.ToArray(), lines.Count - 3);

                if (point == lines.Count() - 1) break;

                Parking.GetParking().Add(vehicles[point]);
                vehicles.Remove(vehicles[point]);
                VehicleManager.Write(vehicles);
            }
        }
        private void ShowTransactions()
        {
            Console.Clear();
            var step = new string(' ', 5); 
            foreach (var line in Parking.GetParking().Transactions.Select(transaction => step + transaction))
                Console.WriteLine(line);
            Console.Write("\n --> BACK"); Console.ReadKey();
        }
        #endregion
        #region Parking Settings
        private ParkingSettings newSettings = SettingsManager.Read();
        private void ParkingSettings()
        {
            while (true)
            {
                var changes = false;
                var settings = SettingsManager.Read();
                var list = new List<string>
                {
                    "Maximum Capacity (" + settings.MaximumCapacity + (settings.MaximumCapacity == newSettings.MaximumCapacity ? ")" : " --> " + newSettings.MaximumCapacity + ")"),
                    "Initial Balance (" + settings.InitialBalance + (settings.InitialBalance == newSettings.InitialBalance ? ")" : " --> " + newSettings.InitialBalance + ")"),
                    "Fine Coefficient (" + settings.FineCoefficient + (settings.FineCoefficient == newSettings.FineCoefficient ? ")" : " --> " + newSettings.FineCoefficient + ")"),
                    "Payment Periodicity (" + settings.PaymentPeriodicity + (settings.PaymentPeriodicity == newSettings.PaymentPeriodicity ? ")" : " --> " + newSettings.PaymentPeriodicity + ")"),
                    "Bus Tariff (" + settings.BusTariff + (settings.BusTariff == newSettings.BusTariff ? ")" : " --> " + newSettings.BusTariff + ")"),
                    "Car Tariff (" + settings.CarTariff + (settings.CarTariff == newSettings.CarTariff ? ")" : " --> " + newSettings.CarTariff + ")"),
                    "Motorcycle Tariff (" + settings.MotorcycleTariff + (settings.MotorcycleTariff == newSettings.MotorcycleTariff ? ")" : " --> " + newSettings.MotorcycleTariff + ")"),
                    "Truck Tariff (" + settings.TruckTariff + (settings.TruckTariff == newSettings.TruckTariff ? ")" : " --> " + newSettings.TruckTariff + ")"),
                };

                foreach (var line in list)
                    if (line.Contains("-->"))
                        changes = true;

                if (changes)
                {
                    list.Add("APPLY CHANGES");
                    list.Add("RESET");
                }
                list.Add("BACK");

                var point = NavigationList("PARKING SETTINGS", list.ToArray(), list.Count - 2);

                switch (point)
                {
                    case 0:
                        MaximumCapacity();
                        continue;
                    case 1:
                        InitialBalance();
                        continue;
                    case 2:
                        FineCoefficient();
                        continue;
                    case 3:
                        PaymentPeriodicity();
                        continue;
                    case 4:
                        BusTariff();
                        continue;
                    case 5:
                        CarTariff();
                        continue;
                    case 6:
                        MotorcycleTariff();
                        continue;
                    case 7:
                        TruckTariff();
                        continue;
                    default:
                        break;
                }
                if (changes)
                {
                    if (point == list.Count - 3)
                    {
                        ApplyChanges();
                        continue;
                    }
                    if (point == list.Count - 2)
                    {
                        newSettings = SettingsManager.Read();
                        continue;
                    }
                }
                if (point == list.Count - 1)
                    break;

                break;
            }
        }
        private void MaximumCapacity()
        {
            while (true)
            {
                Console.Clear();
                Console.Write("Maximum Capacity (0 < x <= 100): ");
                var MaximumCapacity = int.Parse(Console.ReadLine());
                if (MaximumCapacity <= 0 && MaximumCapacity > 100)
                {
                    Console.BackgroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine("\nIncorrect input - 0 < x <= 100");
                    Console.BackgroundColor = ConsoleColor.DarkGray;
                    continue;
                }
                newSettings.MaximumCapacity = MaximumCapacity;
                break;
            }
            Console.ReadKey();
        }
        private void InitialBalance()
        {
            while (true)
            {
                Console.Clear();
                Console.Write("Initial Balance (0.0 <= x): ");
                var InitialBalance = double.Parse(Console.ReadLine());
                if (InitialBalance < 0)
                {
                    Console.BackgroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine("\nIncorrect input - 0.0 <= x");
                    Console.BackgroundColor = ConsoleColor.DarkGray;
                    continue;
                }
                newSettings.InitialBalance = InitialBalance;
                break;
            }
            Console.ReadKey();
        }
        private void FineCoefficient()
        {
            while (true)
            {
                Console.Clear();
                Console.Write("Fine Coefficient (0.0 < x): ");
                var FineCoefficient = double.Parse(Console.ReadLine());
                if (FineCoefficient < 0.0)
                {
                    Console.BackgroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine("\nIncorrect input - 0.0 < x");
                    Console.BackgroundColor = ConsoleColor.DarkGray;
                    continue;
                }
                newSettings.FineCoefficient = FineCoefficient;
                break;
            }
            Console.ReadKey();
        }
        private void PaymentPeriodicity()
        {
            while (true)
            {
                Console.Clear();
                Console.Write("Payment Periodicity (0 < x): ");
                var PaymentPeriodicity = int.Parse(Console.ReadLine());
                if (PaymentPeriodicity <= 0)
                {
                    Console.BackgroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine("\nIncorrect input - 0 <= x");
                    Console.BackgroundColor = ConsoleColor.DarkGray;
                    continue;
                }
                newSettings.PaymentPeriodicity = PaymentPeriodicity;
                break;
            }
            Console.ReadKey();
        }
        private void BusTariff()
        {
            while (true)
            {
                Console.Clear();
                Console.Write("Bus Tariff (0.0 < x): ");
                var BusTariff = double.Parse(Console.ReadLine());
                if (BusTariff <= 0.0)
                {
                    Console.BackgroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine("\nIncorrect input - 0.0 < x");
                    Console.BackgroundColor = ConsoleColor.DarkGray;
                    continue;
                }
                newSettings.BusTariff = BusTariff;
                break;
            }
            Console.ReadKey();
        }
        private void CarTariff()
        {
            while (true)
            {
                Console.Clear();
                Console.Write("Car Tariff (0.0 < x): ");
                var CarTariff = double.Parse(Console.ReadLine());
                if (CarTariff <= 0.0)
                {
                    Console.BackgroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine("\nIncorrect input - 0.0 < x");
                    Console.BackgroundColor = ConsoleColor.DarkGray;
                    continue;
                }
                newSettings.CarTariff = CarTariff;
                break;
            }
            Console.ReadKey();
        }
        private void MotorcycleTariff()
        {
            while (true)
            {
                Console.Clear();
                Console.Write("Motorcycle Tariff (0.0 < x): ");
                var MotorcycleTariff = double.Parse(Console.ReadLine());
                if (MotorcycleTariff <= 0.0)
                {
                    Console.BackgroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine("\nIncorrect input - 0.0 < x");
                    Console.BackgroundColor = ConsoleColor.DarkGray;
                    continue;
                }
                newSettings.MotorcycleTariff = MotorcycleTariff;
                break;
            }
            Console.ReadKey();
        }
        private void TruckTariff()
        {
            while (true)
            {
                Console.Clear();
                Console.Write("Truck Tariff (0 < x): ");
                var TruckTariff = double.Parse(Console.ReadLine());
                if (TruckTariff <= 0 && TruckTariff > 100)
                {
                    Console.BackgroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine("\nIncorrect input - 0 < x <= 100");
                    Console.BackgroundColor = ConsoleColor.DarkGray;
                    continue;
                }
                newSettings.TruckTariff = TruckTariff;
                break;
            }
            Console.ReadKey();
        }
        private void ApplyChanges()
        {
            SettingsManager.Write(newSettings);
        }
        #endregion
        #region Navigation
        private static int NavigationList(string title, string[] items, int stepAfter = -1, string pointer = "-->")
        {
            pointer = " " + pointer + " ";
            var step = new string(' ', pointer.Length);
            var active = 0;

            while (true)
            {
                Console.Clear();
                Console.WriteLine(step + title);
                for (var i = 0; i < items.Length; i++)
                {
                    Console.WriteLine((i == active ? pointer : step) + items[i]);
                    if (i == stepAfter + 1) Console.WriteLine();
                }
                var key = Console.ReadKey(true);
                switch (key.Key)
                {
                    case ConsoleKey.UpArrow:
                        active = active == 0 ? items.Length - 1 : active - 1;
                        continue;
                    case ConsoleKey.DownArrow:
                        active = active == items.Length - 1 ? 0 : active + 1;
                        continue;
                    case ConsoleKey.Enter:
                        break;
                    default:
                        continue;
                }
                return active;
            }
        }
        #endregion
    }
}
