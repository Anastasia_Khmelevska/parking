﻿namespace Parking
{
    public class ParkingSettings
    {
        public int MaximumCapacity { get; set; }
        public double InitialBalance { get; set; }
        public double FineCoefficient { get; set; } = 1.5;
        public int PaymentPeriodicity { get; set; }
        public double BusTariff { get; set; } = 3.5;
        public double CarTariff { get; set; } = 2.0;
        public double MotorcycleTariff { get; set; } = 1.0;
        public double TruckTariff { get; set; } = 5.0;
    }
}
