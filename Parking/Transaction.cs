﻿using System;

namespace Parking
{
    public class Transaction
    {
        #region Public Properties
        public int VehicleId { get; private set; }
        public double Paid { get; private set; }
        public DateTime TransactionDateTime { get; private set; }
        #endregion
        #region Constructors
        public Transaction(int vehicleId, double paid)
        {
            VehicleId = vehicleId;
            Paid = paid;
            TransactionDateTime = DateTime.Now;
        }
        #endregion
        #region Public Methods
        public override string ToString() =>
            "Vehicle Id: " + VehicleId +
            " | Paid: " + Paid +
            " | Transaction DateTime: " + TransactionDateTime.ToLongDateString() + " " + TransactionDateTime.ToLongTimeString();
        #endregion
    }
}
