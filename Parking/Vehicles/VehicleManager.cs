﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Parking.Vehicles
{
    public static class VehicleManager
    {
        public static string VehiclesFileName = "vehiclesStorage.vehcl";
        private static string _rootDirectory;
        public static string RootDirectory =>
            _rootDirectory ?? (_rootDirectory = _getRootDirectory("Parking"));
        public static string VehiclesFile =>
            Directory.GetFiles(RootDirectory, VehiclesFileName, SearchOption.AllDirectories)[0];
        private static string _getRootDirectory(string directoryName)
        {
            var directory = new DirectoryInfo(Directory.GetCurrentDirectory());

            while (directory.Name != directoryName)
                try
                {
                    directory = Directory.GetParent(directory.ToString());
                }
                catch (DirectoryNotFoundException exception)
                {
                    throw new DirectoryNotFoundException("Directory not found: " + directoryName + "\n" + exception.Message);
                }

            return directory.ToString();
        }

        public static void Write(IEnumerable<IVehicle> vehicles) =>
            File.WriteAllText(VehiclesFile,
                string.Join('\n', vehicles.Select(vehicle => vehicle.ToString()).ToArray()));
        public static IEnumerable<IVehicle> Read() =>
            File.ReadAllLines(VehiclesFile).Select(line => new Vehicle(line));
    }
}
