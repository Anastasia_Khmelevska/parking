﻿namespace Parking.Vehicles
{
    public enum VehicleTypes
    {
        Car = 0,
        Bus = 1,
        Motorcycle = 2,
        Truck = 3
    }
}
