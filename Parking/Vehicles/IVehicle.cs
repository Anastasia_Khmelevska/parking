﻿namespace Parking.Vehicles
{
    public interface IVehicle
    {
        int Id { get; }
        double Balance { get; }
        VehicleTypes VehicleType { get; }
        double Pay(double price, double fineCoefficient = 1);
        string ToString();
    }
}
