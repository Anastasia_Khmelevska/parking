﻿using System;

namespace Parking.Vehicles
{
    public class Vehicle : IVehicle
    {
        #region Static Properties
        private static int id = 0;
        private static int ID => id++;
        #endregion
        #region Public Properties
        public int Id { get; private set; }
        public double Balance { get; private set; }
        public VehicleTypes VehicleType { get; private set; }
        #endregion
        #region Constructors

        public Vehicle(double initialBalance, VehicleTypes vehicleType)
        {
            Id = ID;
            Balance = initialBalance;
            VehicleType = vehicleType;
        }
        public Vehicle(string stringVehicle)
        {
            var words = stringVehicle.Split(" | ");
            VehicleType = Enum.Parse<VehicleTypes>(words[0].Replace("Type: ", ""));
            Id = int.Parse(words[1].Replace("Id: ", ""));
            Balance = double.Parse(words[2].Replace("Balance: ", ""));
        }

        #endregion
        #region Public Methods
        public double Pay(double price, double fineCoefficient = 1)
        {
            if (Balance - price < 0)
            {
                var _price = price * fineCoefficient;
                Balance -= _price;
                return _price;
            }
            Balance -= price;
            return price;
        }
        public override string ToString() =>
            "Type: " + VehicleType.ToString() + " | Id: " + Id + " | Balance: " + Balance;
        #endregion
    }
}
