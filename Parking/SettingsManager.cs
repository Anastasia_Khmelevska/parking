﻿using System.IO;
using Newtonsoft.Json;

namespace Parking
{
    public static class SettingsManager
    {
        #region Properties
        public static string SettingsFileName = "appsettings.json";
        private static string _rootDirectory;
        public static string RootDirectory =>
            _rootDirectory ?? (_rootDirectory = _getRootDirectory("Parking"));
        public static string SettingsFile =>
            Directory.GetFiles(RootDirectory, SettingsFileName, SearchOption.AllDirectories)[0];
        #endregion
        #region Public Methods
        public static ParkingSettings Read() =>
            JsonConvert.DeserializeObject<ParkingSettings>(File.ReadAllText(SettingsFile));
        public static bool Write(ParkingSettings settings)
        {
            var json = JsonConvert.SerializeObject(settings);
            File.WriteAllText(SettingsFile, json);
            return true;
        }
        #endregion
        #region Private Methods
        private static string _getRootDirectory(string directoryName)
        {
            var directory = new DirectoryInfo(Directory.GetCurrentDirectory());

            while (directory.Name != directoryName)
                try
                {
                    directory = Directory.GetParent(directory.ToString());
                }
                catch (DirectoryNotFoundException exception)
                {
                    throw new DirectoryNotFoundException("Directory not found: " + directoryName + "\n" + exception.Message);
                }

            return directory.ToString();
        }
        #endregion
    }
}
